/*
 * Copyright 2013-2014 Colby Skeggs
 * 
 * This file is part of the CCRE, the Common Chicken Runtime Engine.
 * 
 * The CCRE is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * The CCRE is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CCRE.  If not, see <http://www.gnu.org/licenses/>.
 */
package ccre.ctrl;

import ccre.channel.BooleanInput;
import ccre.channel.BooleanInputPoll;
import ccre.channel.BooleanOutput;
import ccre.channel.FloatInput;
import ccre.channel.FloatInputPoll;
import ccre.channel.FloatOutput;
import ccre.ctrl.MixingImpls.BCF;
import ccre.ctrl.MixingImpls.BCF2;
import ccre.ctrl.MixingImpls.BSF;
import ccre.ctrl.MixingImpls.BSF2;
import ccre.ctrl.MixingImpls.BooleanSelectFloatImpl;
import ccre.ctrl.MixingImpls.QuadSelectImpl;
import ccre.ctrl.MixingImpls.QuadSelectImpl2;

/**
 * Mixing is a class that provides a wide variety of useful static methods to
 * accomplish various common actions using channels, that don't fall into the
 * existing categories of EventMixing, FloatMixing, or BooleanMixing.
 *
 * Common actions involving teleoperating the robot can be found in DriverImpls.
 *
 * @see DriverImpls
 * @see BooleanMixing
 * @see FloatMixing
 * @see EventMixing
 * @author skeggsc
 */
public class Mixing {

    /**
     * Sets the given FloatOutput to one of two values depending on what was
     * written to the returned BooleanOutput.
     *
     * This would be useful if a motor could only be 75% power or 0% power and
     * you wanted to control it using a boolean.
     *
     * @param controlled the FloatOutput to controll.
     * @param off the value to send if the boolean is false.
     * @param on the value to send if the boolean is true.
     * @return the BooleanOutput that will now control the provided FloatOutput.
     */
    public static BooleanOutput select(final FloatOutput controlled, final float off, final float on) {
        return new BSF(controlled, off, on);
    }

    /**
     * Provides a FloatInput that contains a value selected from the two float
     * arguments based on the state of the specified BooleanInput.
     *
     * @param selector the value to select the float value based on.
     * @param off the value to use when false
     * @param on the value to use when true
     * @return the FloatInput calculated from the selector's value and the two
     * floats.
     */
    public static FloatInput select(BooleanInput selector, float off, float on) {
        BCF out = new BCF(selector.get(), off, on);
        selector.send(out);
        return out;
    }

    /**
     * Provides a FloatInput that contains a value selected from the two float
     * arguments based on the state of the specified BooleanInputProducer.
     *
     * @param selector the value to select the float value based on.
     * @param default_ the value to assume for the BooleanInputProducer before
     * any changes are detected.
     * @param off the value to use when false
     * @param on the value to use when true
     * @return the FloatInput calculated from the selector's value and the two
     * floats.
     */
    public static FloatInput select(BooleanInput selector, boolean default_, float off, float on) {
        BCF out = new BCF(default_, off, on);
        selector.send(out);
        return out;
    }

    /**
     * Provides a FloatInputPoll that contains a value selected from the two
     * float arguments based on the state of the specified BooleanInputPool.
     *
     * @param selector the value to select the float value based on.
     * @param off the value to use when false
     * @param on the value to use when true
     * @return the FloatInputPoll calculated from the selector's value and the
     * two floats.
     */
    public static FloatInputPoll select(BooleanInputPoll selector, float off, float on) {
        return new BSF2(selector, off, on);
    }

    /**
     * The returned BooleanOutput is a way to modify the specified target. When
     * the BooleanOutput is changed, the target is set to the current value of
     * the associated parameter (the on parameter if true, the off parameter if
     * false).
     *
     * Warning: changes to the FloatInputPoll parameters will not modify the
     * output until the BooleanOutput is written to!
     *
     * @param target the FloatOutput to write to.
     * @param off the value to write if the written boolean is false.
     * @param on the value to write if the written boolean is true.
     * @return the BooleanOutput that will modify the specified target.
     */
    public static BooleanOutput select(final FloatOutput target, final FloatInputPoll off, final FloatInputPoll on) {
        return new BooleanOutput() {
            public void set(boolean value) {
                target.set(value ? on.get() : off.get());
            }
        };
    }

    /**
     * Returns a FloatInput with a value selected from two FloatInputPolls based
     * on the BooleanInput's value.
     *
     * Warning: changes to the FloatInputPoll parameters will not modify the
     * output until the BooleanInput changes!
     *
     * @param selector the selector to choose an input using.
     * @param off if the selector is false.
     * @param on if the selector is true.
     * @return the value selected based on the selector's value and the statuses
     * of the two arguments.
     */
    public static FloatInput select(BooleanInput selector, FloatInputPoll off, FloatInputPoll on) {
        BCF2 out = new BCF2(selector.get(), off, on);
        selector.send(out);
        return out;
    }

    /**
     * Returns a FloatInput with a value selected from two FloatInputPolls based
     * on the BooleanInputProducer's value.
     *
     * Warning: changes to the FloatInputPoll parameters will not modify the
     * output until the BooleanInputProducer changes!
     *
     * @param selector the selector to choose an input using.
     * @param default_ the value to default the selector to before it changes.
     * @param off if the selector is false.
     * @param on if the selector is true.
     * @return the value selected based on the selector's value and the statuses
     * of the two arguments.
     */
    public static FloatInput select(BooleanInput selector, boolean default_, FloatInputPoll off, FloatInputPoll on) {
        BCF2 out = new BCF2(default_, off, on);
        selector.send(out);
        return out;
    }

    /**
     * Returns a FloatInputPoll with a value selected from two specified
     * FloatInputPolls based on the BooleanInputPoll's value.
     *
     * @param selector the selector to choose an input using.
     * @param off if the selector is false.
     * @param on if the selector is true.
     * @return the value selected based on the selector's value and the statuses
     * of the two arguments.
     */
    public static FloatInputPoll select(final BooleanInputPoll selector, final FloatInputPoll off, final FloatInputPoll on) {
        return new BooleanSelectFloatImpl(selector, on, off);
    }

    /**
     * Returns a four-way select based on two BooleanInputPolls from four
     * floats.
     *
     * @param alpha The first boolean.
     * @param beta The second boolean.
     * @param ff The value to use when both inputs are false.
     * @param ft The value to use when the first is false and the second is
     * true.
     * @param tf The value to use when the first is true and the second is
     * false.
     * @param tt The value to use when both inputs are true.
     * @return The FloatInputPoll representing the current value.
     */
    public static FloatInputPoll quadSelect(final BooleanInputPoll alpha, final BooleanInputPoll beta, final float ff, final float ft, final float tf, final float tt) {
        return new QuadSelectImpl(alpha, beta, tt, tf, ft, ff);
    }

    /**
     * Returns a four-way select based on two BooleanInputPolls from four
     * FloatInputPolls.
     *
     * @param alpha The first boolean.
     * @param beta The second boolean.
     * @param ff The value to use when both inputs are false.
     * @param ft The value to use when the first is false and the second is
     * true.
     * @param tf The value to use when the first is true and the second is
     * false.
     * @param tt The value to use when both inputs are true.
     * @return The FloatInputPoll representing the current value.
     */
    public static FloatInputPoll quadSelect(final BooleanInputPoll alpha, final BooleanInputPoll beta, final FloatInputPoll ff, final FloatInputPoll ft, final FloatInputPoll tf, final FloatInputPoll tt) {
        return new QuadSelectImpl2(alpha, beta, tt, tf, ft, ff);
    }

    private Mixing() {
    }
}
